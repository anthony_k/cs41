/*
 * WeightList.h
 *
 *  Created on: Oct 6, 2014
 *      Author: Anthony
 */

#ifndef WEIGHTLIST_H_
#define WEIGHTLIST_H_

#include <list>
#include <string>
using namespace std;

class WeightList
{
public:
	WeightList();
	~WeightList();
	WeightList BuildList();


private:
	string name;
	int weight;
};





#endif /* WEIGHTLIST_H_ */
