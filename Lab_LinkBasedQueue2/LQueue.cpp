/*
 * LQueue.cpp
 *
 *  Created on: Apr 11, 2015
 *      Author: anthony
 */
#include "LQueue.h"
#include<iostream>
using namespace std;

int main()
{
	int n;
	Queue<int> q;

	cout << "1. QUEUE\n2. DEQUEUE\n3. ISEMPTY\n4. QUIT\n";
	cin >> n;

	while(n != 4)
	{

		switch(n)
		{
		case 1 :
			cout << "ENQUEUE: ";
			cin >> n;
			q.Enqueue(n);
			break;
		case 2 :
			n = q.Dequeue();
			cout << "DEQUEUED: " << n << endl;
			break;
		case 3 :
			if(q.IsEmpty())
			{
				cout << "EMPTY!\n";
			}
			else
			{
				cout <<"NOT EMPTY!\n";
			}

			break;
		default:
			cout << "INVALID INPUT!\n";
			break;
		}

		cout << "1. QUEUE\n2. DEQUEUE\n3. ISEMPTY\n4. QUIT\n";
		cin >> n;
	}
	return 0;
}



