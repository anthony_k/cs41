/*
 * AUTHOR: Anthony Koutroulis
 * DUE: 4/12/15
 * LAB QUEUE
 */

#ifndef LQUEUE_H_
#define LQUEUE_H_
#define NULL 0
#include<iostream>


template<typename Type>
struct QNode
{
	Type data;
	QNode* next;
	QNode* prev;

	QNode(Type d){
		data = d;
		prev = NULL;
		next = NULL;
	}
	QNode(Type d, QNode* p){
		data = d;
		prev = p;
		next = NULL;
	}
};

template<typename Type>
class Queue
{
public:
	Queue(){
		front = NULL;
		back = NULL;
		count = 0;
	}

	void Enqueue(Type data){
		if(IsEmpty())
		{
			front = new QNode<Type>(data);
			back = front;
		}
		else
		{
			back->next = new QNode<Type>(data,back);
			back = back->next;
		}
		count++;
	}

	Type Dequeue(){
		if(IsEmpty())
		{
			std::cerr << "QUEUE IS EMPTY";
		}
		else
		{
			Type data;
			Dequeue(data);
			return data;
		}
	}

	void Dequeue(Type& data){
		data = back->data;

		QNode<Type>* it = back;
		back = back->prev;
		delete it;
		count--;
	}

	bool IsEmpty(){
		return count == 0;
	}

private:
	QNode<Type>* front;
	QNode<Type>* back;
	int count;
};





#endif /* LQUEUE_H_ */
