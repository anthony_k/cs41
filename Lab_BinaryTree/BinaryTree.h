/**************************************************************************
 * LAB: BinaryTree
 * AUTHOR: Anthony Koutroulis
 *************************************************************************/

#ifndef BINARYTREE_H_
#define BINARYTREE_H_

#ifndef BASE_AR_SIZE
#define BASE_AR_SIZE 5
#endif	/* BASE_AR_SIZE */


#include <iostream>



template<class Type>
class BinaryTree
{
public:
	BinaryTree();
	BinaryTree(const unsigned int);
	~BinaryTree();
	void Add(Type);
	void Print();
	void PreOrder(unsigned int); // prints the node
	void InOrder(unsigned int);
	void PostOrder(unsigned int);

private:
	bool IsFull() const;
	void ResizeArray();
	unsigned int count;
	unsigned int ar_size;

	struct Node
	{
		Type data;
		bool exists;
	};

	Node* data_ar;

};

#endif /* BINARYTREE_H_ */
