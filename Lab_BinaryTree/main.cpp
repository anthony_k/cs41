/*
 * main.cpp
 *
 *  Created on: Mar 4, 2015
 *      Author: Anthony
 */

#include "BinaryTree.h"
#include "BinaryTree.cpp"
#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
	BinaryTree<int> bt(3);
	int input;

	cout << "1. ADD\n2. PRINT\n3. PREORDER\n";
	cin >> input;
	cout << endl;

	while(input == 1 || input == 2 || input == 3)
	{

		if(input == 1)
		{
			cout << "ENTER: ";
			cin >> input;
			bt.Add(input);
		}
		else if(input == 2)
		{
			bt.Print();
			cout << endl;
		}
		else
		{
			bt.PreOrder(0);
		}

		cout << "1. ADD\n2. PRINT\n3. PREORDER\n";
		cin >> input;
		cout << endl;

	}

}


