#include "BinaryTree.h"

template<class Type>
BinaryTree<Type>::BinaryTree()
{
	ar_size = BASE_AR_SIZE;

	data_ar = new Node[ar_size];

	for(unsigned int i = 0; i < ar_size; i++)
	{
		data_ar[i].data = 0;
		data_ar[i].exists = false;
	}

	count = 0;

}

template<class Type>
BinaryTree<Type>::BinaryTree(const unsigned int AR_SIZE)
{
	ar_size = AR_SIZE;

	data_ar = new Node[ar_size];

	for(unsigned int i = 0; i < ar_size; i++)
	{
		data_ar[i].data = 0;
		data_ar[i].exists = false;
	}

	count = 0;

}

template<class Type>
BinaryTree<Type>::~BinaryTree()
{
	delete [] data_ar;
	data_ar = 0;
	ar_size = 0;
	count = 0;
}

template<class Type>
void BinaryTree<Type>::Add(Type data)
{
	if(IsFull())
	{
		ResizeArray();
	}
	data_ar[count].data = data;
	data_ar[count].exists = true;
	count++;
}

template<class Type>
void BinaryTree<Type>::Print()
{
	for(unsigned int i = 0; i < count; i++)
		std::cout << data_ar[i].data << std::endl;
}

template<class Type>
void BinaryTree<Type>::PreOrder(unsigned int i)
{
#define LEFT 2*i+1
#define RIGHT 2*i+1

	std::cout << data_ar[i].data << std::endl;

	if(data_ar[LEFT].exists)
	{
		PreOrder(LEFT);
	}

	if(data_ar[RIGHT].exists)
	{
		PreOrder(RIGHT);
	}

}

template<class Type>
void BinaryTree<Type>::InOrder(unsigned int i)
{
#define LEFT 2*i+1
#define RIGHT 2*i+1

	if(data_ar[LEFT].exists)
	{
		PreOrder(LEFT);
	}

	std::cout << data_ar[i].data << std::endl;

	if(data_ar[RIGHT].exists)
	{
		PreOrder(RIGHT);
	}
}

template<class Type>
void BinaryTree<Type>::PostOrder(unsigned int i)
{
#define LEFT 2*i+1
#define RIGHT 2*i+1


	if(data_ar[LEFT].exists)
	{
		PreOrder(LEFT);
	}

	if(data_ar[RIGHT].exists)
	{
		PreOrder(RIGHT);
	}

	std::cout << data_ar[i].data << std::endl;

}


template<class Type>
bool BinaryTree<Type>::IsFull() const
{
	return count == ar_size;
}

template<class Type>
void BinaryTree<Type>::ResizeArray()
{
	ar_size *= 10;

	Node* tempAr = new Node[ar_size];

	for(unsigned int i = 0; i < ar_size; i++)
	{
		while(i < count)
		{
			tempAr[i].data = data_ar[i].data;
			tempAr[i].exists = true;
			i++;
		}

		tempAr[i].data = 0;
		tempAr[i].exists = false;
	}

	delete [] data_ar;
	data_ar = tempAr;
	tempAr = 0;
	delete [] tempAr;
}

template class BinaryTree<int>;
