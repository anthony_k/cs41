/***********************************************************************************
 *  Create a list based graph representation.
 *  It will need to support the following operations.
 *  Ask the user how many points there are.
 *  Ask the user to label those points, ie "A", "B", "C"...
 *  Define the graph as an array of linked lists based on the number of 
 *   points that holds labels. The node type would support the label and weight.
 *  Repeatedly ask the user to define edges between two points with the 
 *   weight of the edge. Add these edges to the list.
 *  Have a list method that will list out all of the edges in the graph 
 *   with their weights.
 **********************************************************************************/
 #ifndef _ADJACENCYLIST_H
 #define _ADJACENCYLIST_H
 #include <iostream>
 
 template<typename VType>
 struct Vertex
 {
    VType label;

    struct Edge
    {
        Vertex* source;
        Vertex* destination;
        int weight;
    };
    
    Edge* link;
};

class AdjacencyList
{
public:
    GraphList()
    {
        std::cout << "Enter number of points: ";
        std::cin >> size;
        
        if(size > 0)
        {
            adjacency_list = new Vertex*[size];
            for(int i = 0; i < size; i++)
            {
                adjacency_list[i] = new Vertex[size];
                for(int j = 0; j < size; j++)
                {
                    adjacency_list[i][j] = new Vertex;
                }
            }
        }
    
private:
    Vertex** adjacency_list;
    int size;
};

 #endif