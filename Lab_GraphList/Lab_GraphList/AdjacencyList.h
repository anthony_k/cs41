/***********************************************************************************
 *  Create a list based graph representation.
 *  It will need to support the following operations.
 *  Ask the user how many points there are.
 *  Ask the user to label those points, ie "A", "B", "C"...
 *  Define the graph as an array of linked lists based on the number of 
 *   points that holds labels. The node type would support the label and weight.
 *  Repeatedly ask the user to define edges between two points with the 
 *   weight of the edge. Add these edges to the list.
 *  Have a list method that will list out all of the edges in the graph 
 *   with their weights.
 *
 *   AUTHOR: Anthony Koutroulis
 **********************************************************************************/
#ifndef _ADJACENCYLIST_H
#define _ADJACENCYLIST_H
#include <iostream>
#include <string>

struct Edge
{
	Edge(int w, int i)
	{
		destination_index = i;
		weight = w;
		next = NULL;
	}

	Edge(int w, int i, Edge* n)
	{
		destination_index = i;
		weight = w;
		next = n;
	}

	int destination_index;
	int weight;
	Edge* next;
};

struct Vertex
{
	std::string label;
	Edge* next_edge;
};

class AdjacencyList
{
public:
	AdjacencyList()
	{
		it = NULL;
		size = 0;
		std::cout << "Enter number of points: ";
		std::cin >> size;
		std::cin.ignore(10000,'\n');

		if(size > 0)
		{
			adjacency_list = new Vertex[size];
			for(int i = 0; i < size; i++)
			{
				std::cout << "Enter label for vertex at index " << i << ": ";
				std::getline(std::cin, adjacency_list[i].label);
				adjacency_list[i].next_edge = NULL;
			}
		}
	}

	void List()
	{


		for(int i = 0; i < size; i++)
		{
			it = adjacency_list[i].next_edge;
			while(it != NULL)
			{
				std::cout << adjacency_list[i].label << " --";
				std::cout << it->weight << "--> ";
				std::cout << adjacency_list[it->destination_index].label;
				std::cout << '\n';

				it = it->next;
			}
		}

	}

	void set_edge(int source, int destination, int weight)
	{
		it = adjacency_list[source].next_edge;
		if(it == NULL)
		{
			adjacency_list[source].next_edge = new Edge(weight,destination);
		}
		else if(weight < it->weight)
		{
			adjacency_list[source].next_edge = new Edge(weight,destination,it);
		}
		else
		{
			bool found = false;

			while(it->next != NULL && !found)
			{
				if(weight < it->next->weight)
				{
					found = true;
				}
				else
				{
					it = it->next;
				}
			}
			it->next = new Edge(weight,destination,it->next);
		}
		it = NULL;
	}




private:
	Vertex* adjacency_list;
	Edge* it;
	int size;
};

#endif
