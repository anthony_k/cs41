/*
 * AdjacencyList.cpp
 *
 *  Created on: Apr 11, 2015
 *      Author: anthony
 */
#include "AdjacencyList.h"
#include <iostream>
#include <string>

using namespace std;

int main()
{
	AdjacencyList list;
	int s;
	int d;
	int w;

	cout << "\nEnter source index: ";
	cin >> s;

	while(s != -1)
	{
		cout << "Enter destination index: ";
		cin >> d;

		cout << "Enter weight for edge: ";
		cin >> w;

		list.set_edge(s,d,w);

		cout << "::EDGES::\n";
		list.List();

		cout << "\nEnter source index: ";
		cin >> s;
	}

	return 0;
}


