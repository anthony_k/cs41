
#include "Record_BST.h"

Record_BST::Record_BST() {
	root = NULL;
	node = NULL;
}

Record_BST::~Record_BST() {
	// Calls recursive destructor: ~BST_Node()
	delete root;
}

void Record_BST::Add(Record data) {
	node = new BST_Node(data);

	if (node == NULL) {
		std::cout << "INSUFFICIENT MEMORY\n";
	} else {
		// Determine where the node may go
		if (root == NULL) {
			// First case, set to root
			root = node;
			node = NULL;
		} else if (node->data->name < root->data->name) {
			// Start down left subtree
			node->parent = root;
			Add_Sorted(root->left);
		} else if (node->data->name > root->data->name) {
			// Start down right subtree
			node->parent = root;
			Add_Sorted(root->right);
		} else // node->data == root->data
		{
			// Already in tree, discard
			delete node;
			node = NULL;
		}

	}
}

void Record_BST::Add_Sorted(BST_Node*& child) {
	if (child == NULL) {
		// Place at this leaf
		child = node;
	} else if (node->data->name < child->data->name) {
		// Traverse left
		node->parent = child;
		Add_Sorted(child->left);
	} else if (node->data->name > child->data->name) {
		// Traverse right
		node->parent = child;
		Add_Sorted(child->right);
	} else // node->data == root->data
	{
		// Free memory
		delete node;
	}
}

void Record_BST::Print(Traversal type) {
	if (root == NULL) {
		std::cout << "NOTHING TO PRINT\n";
	} else {
		node = root;

		switch (type) {
		case PRE:
			Print_PreOrder();
			break;
		case IN:
			// Recursively prints in-order
			Print_InOrder();
			break;
		case POST:
			Print_PostOrder();
			break;

		default:
			break;
		}

	}

}

void Record_BST::Print_PreOrder() {
	std::cout << node->data->name << '\t';

	if (node->left != NULL) {
		node = node->left;
		Print_PreOrder();
	}

	if (node->right != NULL) {

		node = node->right;
		Print_PreOrder();
	}

	node = node->parent;

	return;
}

void Record_BST::Print_InOrder() {
	if (node->left != NULL) {
		node = node->left;
		Print_InOrder();
	}

	std::cout << node->data->name << '\t';

	if (node->right != NULL) {

		node = node->right;
		Print_InOrder();
	}

	node = node->parent;

	return;
}

void Record_BST::Print_PostOrder() {
	if (node->left != NULL) {
		node = node->left;
		Print_PostOrder();
	}

	if (node->right != NULL) {

		node = node->right;
		Print_PostOrder();
	}

	std::cout << node->data->name << '\t';
	node = node->parent;

	return;
}

bool Record_BST::IsLeaf(BST_Node* n) {
	return (n->left == NULL && n->right == NULL);
}

int Record_BST::GetHeight() {
	return GetHeight(root);
}

int Record_BST::GetHeight(BST_Node* n) {
	if (n == NULL) {
		return 0;
	} else {
		return std::max(GetHeight(n->left), GetHeight(n->right)) + 1;
	}
}

int Record_BST::GetNumLeaves() {
	int leaves = 0;
	return GetNumLeaves(root, leaves);
}

int Record_BST::GetNumLeaves(BST_Node* n, int& leaves) {
	if (n == NULL) {
		return 0;
	} else if (IsLeaf(n)) {
		leaves = leaves + 1;
	}
	GetNumLeaves(n->left, leaves);
	GetNumLeaves(n->right, leaves);

	return leaves;
}

int Record_BST::LookupWeight(std::string name) {
	return LookupWeight(name, root);
}

int Record_BST::LookupWeight(std::string name, BST_Node* n) {
	if (n == NULL) {
		return -1;
	} else if (name < n->data->name) {
		return LookupWeight(name, n->left);
	} else if (name > n->data->name) {
		return LookupWeight(name, n->right);
	} else {
		return n->data->weight;
	}

}

int Record_BST::GetLowestWeight() {
	int weight = 999;
	GetLowestWeight(root, weight);
	return weight;
}

void Record_BST::GetLowestWeight(BST_Node* n, int& weight) {
	if (n->left != NULL) {
		GetLowestWeight(n->left, weight);
	}

	if (n->right != NULL) {
		GetLowestWeight(n->right, weight);
	}

	if (n->data->weight < weight) {
		weight = n->data->weight;
	}

	return;
}

std::string Record_BST::GetFirstName() {
	node = root;
	while (node->left != NULL) {
		node = node->left;
	}
	return node->data->name;
}
