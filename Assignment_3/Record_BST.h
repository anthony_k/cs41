/*
 * AUTHOR: anthony koutroulis
 */

#ifndef RECORD_BST_H_
#define RECORD_BST_H_

#include<iostream>
#include<string>


struct Record
{
	std::string name;
	int weight;
};

struct BST_Node
{
	Record* data;			// data field
	BST_Node* parent;	// pointer to parent
	BST_Node* left;		// pointer to left child
	BST_Node* right;	// pointer to right child

	BST_Node(Record key)
	{
		left = right = parent = NULL;
		data = new Record;
		data->name = key.name;
		data->weight = key.weight;
	}
	~BST_Node()
	{
		delete left;	// recursively delete left subtree
		delete right;	// recursively delete right subtree
	}
};

enum Traversal
{
	PRE,
	IN,
	POST
};

class Record_BST
{
public:
	Record_BST();
	~Record_BST();

	void Add(Record);	// Adds the data to the tree in sorted order
	void Print(Traversal);	// Prints all the nodes using in-order traversal
	int GetHeight();
	int GetNumLeaves();
	int LookupWeight(std::string);
	int GetLowestWeight();
	std::string GetFirstName();






private:
	bool IsLeaf(BST_Node*);
	void Print_PreOrder();		// recursive preorder print
	void Print_InOrder();				// recursive inorder print
	void Print_PostOrder();
	void Add_Sorted(BST_Node*&);	// recursively find spot for new node
	int GetHeight(BST_Node*);
	int GetNumLeaves(BST_Node*,int&);
	int LookupWeight(std::string,BST_Node*);
	void GetLowestWeight(BST_Node* n, int& weight);

	BST_Node* root;		// Node field for the topmost member
	BST_Node* node;		// Node field for new members
};


#endif /* RECORD_BST_H_ */
