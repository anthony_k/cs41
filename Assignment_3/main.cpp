#include "Record_BST.h"
#include <iostream>
#include <string>

using namespace std;

int main()
{
	string name;
	Record_BST bst;
	Record guy;


	for(int i = 1; i <= 9; i++)
	{
		cout << "Enter name " << i << ": ";
		getline(cin,guy.name);
		guy.weight = 100 - i;
		cout << "Enter " << guy.name << "'s weight: ";
		cin >> guy.weight;
		cin.ignore(10000,'\n');

		bst.Add(guy);
	}

	cout << endl << "Pre-order Traversal: ";
	bst.Print(PRE);

	cout << endl << "In-order Traversal: ";
	bst.Print(IN);

	cout << endl << "Post-order Traversal: ";
	bst.Print(POST);

	cout << endl << "Height: ";
	cout << bst.GetHeight();

	cout << endl << "Leaves: ";
	cout << bst.GetNumLeaves();

	cout << endl << "Who do you want to lookup? ";
	getline(cin, name);
	cout << name << ": " << bst.LookupWeight(name);

	cout << endl << "Lowest weight: ";
	cout << bst.GetLowestWeight();

	cout << endl << "First name (alphabetically): ";
	cout << bst.GetFirstName();


	cin.get();


	return 0;
}

