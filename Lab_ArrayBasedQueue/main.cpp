/*
 * main.cpp
 *
 *  Created on: Mar 1, 2015
 *      Author: Anthony
 */
#include "ArrayQueue.h"
#include "ArrayQueue.cpp"
#include <iostream>

using namespace std;

int main()
{
	ArrayQueue<int> myQue;
	int input;
	input = 0;



	cout << "1,2: ";

	cin >> input;
	while (input > 0)
	{
		if (input == 1)
		{
			cout << "ENTER: ";
			cin >> input;
			myQue.Enqueue(input);
		}
		else
		{
			cout << endl;
			cout << myQue.Dequeue();
			cout << endl;
		}

		cout << "1,2: ";
		cin >> input;
	}

	return 0;
}



