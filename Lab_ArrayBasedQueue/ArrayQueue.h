//////////////////////////////////////////////////////////////////////////
//Create a class/struct.
//
//Members:
//MaxSize const = 10
//Define an array that holds 10 items.
//Count - indicates how many items are on the queue.
//Front - where the front of the queue is in the array.
//Back - Where the end of the queue is in the array.
//
//Methods:
//En-queue
//- Accepts a number and adds to the end of the queue.
//- If the queue is full emit an error indicating full.
//De-queue
//- Returns a number from the front of the queue.
//- If the queueis empty, emit an error indicating the queueis empty.
//IsEmpty
//- Returns a boolean indicating if the queue is empty.
//
//*Note that the front and back can wrap around the end of the array over
//time.  I.e. when the "back" of the queue gets to the last valid array
//index, the next insert will be into array index 0.
//////////////////////////////////////////////////////////////////////////

#ifndef ARRAYQUEUE_H_
#define ARRAYQUEUE_H_

template <class Type>
class ArrayQueue
{
public:
	ArrayQueue();
	~ArrayQueue();
	void Enqueue(Type);
	Type Dequeue();
	bool IsEmpty() const;
	int MaxSize() const;

private:
	const int MAX_SIZE = 10;
	unsigned int front;
	unsigned int back;
	Type* queue;
	int count;
};




#endif /* ARRAYQUEUE_H_ */
