#include "ArrayQueue.h"
#include <iostream>


template <class Type>
ArrayQueue<Type>::ArrayQueue()
{
	queue = new Type[MAX_SIZE];
	front = 0;
	back = 0;
	count = 0;
}

template <class Type>
ArrayQueue<Type>::~ArrayQueue()
{
	delete [] queue;
	queue = NULL;
}

template <class Type>
void ArrayQueue<Type>::Enqueue(Type data)
{
	if (count == MaxSize())
	{
		std::cerr << "ERROR: Queue is full.\n"
				<< data << " was not added!\n";
	}
	else
	{
		count++;
		if (back == MaxSize())
		{
			back = 0;
			queue[back] = data;
		}
		else
		{
			queue[back] = data;
			back++;
		}
	}
}

template <class Type>
Type ArrayQueue<Type>::Dequeue()
{
	if (IsEmpty())
	{
		std::cout << "ERROR: Queue is empty.\n"
				<< "Nothing to dequeue.\n";
		return 0;
	}
	else
	{
		count--;
		Type data;


		if (front == MaxSize())
		{
			front = 0;
			data = queue[front];
		}
		else
		{
			data = queue[front];
			front++;
		}

		return data;
	}
}

template <class Type>
bool ArrayQueue<Type>::IsEmpty() const
{
	return count == 0;
}

template <class Type>
int ArrayQueue<Type>::MaxSize() const
{
	return MAX_SIZE;
}
