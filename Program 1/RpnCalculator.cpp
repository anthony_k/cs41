<<<<<<< HEAD
#include "RpnCalculator.h"

RpnCalculator::RpnCalculator()
{
	opL = 0;
	opR = 0;
}

RpnCalculator::~RpnCalculator()
{
	ClearMemory();
	opL = 0;
	opR = 0;

}

void RpnCalculator::ClearMemory()
{
	memory.Clear();
	opL = 0;
	opR = 0;
}

/*
 * ReadExpression iterates over a string, pausing on a space character,
 * 	then checks the previous block of chars.  If it is a number, the number
 * 	is pushed, if it is a operand character (+,-,*,/,=) it passes to the
 * 	operate function.
 */
void RpnCalculator::ReadExpression(std::string s)
{
	std::string::iterator it;
	std::string::size_type sz;
	it = s.begin();
	sz = 0;

	try
	{
		while(it != s.end())
		{
			it++;
			if(isspace(*it))
			{
				if(isdigit(*(it - 1)))
				{
					sz = it - s.begin();
					memory.Push(atof(s.substr(0,sz).c_str()));
					s = s.substr(sz + 1);
					it = s.begin();
				}
				else
				{

					Operate(*(it - 1));
					s = s.substr(2); // assumes operand is a single char
					it = s.begin();

				}
			}
		}
		Operate(*(it - 1));
	}
	catch(const char* e)
	{
		cout << e;
		memory.Clear();
	}

}

void RpnCalculator::Operate(char c)
{
	// checks for too many operators
	if(c != '=' && memory.GetSize() < 2)
		throw "ERROR: Too many operators";
	switch (c)
	{
	case '+':
		Add();
		break;
	case '-':
		Subtract();
		break;
	case '*':
		Multiply();
		break;
	case '/':
		Divide();
		break;
	case '=':
		Output();
		break;
	}


}

void RpnCalculator::Add()
{
	memory.Pop(opR);
	memory.Pop(opL);
	memory.Push(opL + opR);
}

void RpnCalculator::Subtract()
{
	memory.Pop(opR);
	memory.Pop(opL);
	memory.Push(opL - opR);
}

void RpnCalculator::Multiply()
{
	memory.Pop(opR);
	memory.Pop(opL);
	memory.Push(opL * opR);
}

void RpnCalculator::Divide()
{
	memory.Pop(opR);
	memory.Pop(opL);
	// checks for divide by zero
	if (opR == 0)
		throw "ERROR: Divide by zero";
	memory.Push(opL / opR);
}

void RpnCalculator::Output()
{
	// checks for too many operands
	if(memory.GetSize() > 1)
		throw "ERROR: Too many operands";
	cout << memory.Head();
	memory.Clear();
}


=======
#include "RpnCalculator.h"

RpnCalculator::RpnCalculator()
{
	opL = 0;
	opR = 0;
}

RpnCalculator::~RpnCalculator()
{
	ClearMemory();
	opL = 0;
	opR = 0;

}

void RpnCalculator::ClearMemory()
{
	memory.Clear();
	opL = 0;
	opR = 0;
}

/*
 * ReadExpression iterates over a string, pausing on a space character,
 * 	then checks the previous block of chars.  If it is a number, the number
 * 	is pushed, if it is a operand character (+,-,*,/,=) it passes to the
 * 	operate function.
 */
void RpnCalculator::ReadExpression(std::string s)
{
	std::string::iterator it;
	std::string::size_type sz;
	it = s.begin();
	sz = 0;

	try
	{
		while(it != s.end())
		{
			it++;
			if(isspace(*it))
			{
				if(isdigit(*(it - 1)))
				{
					sz = it - s.begin();
					memory.Push(atof(s.substr(0,sz).c_str()));
					s = s.substr(sz + 1);
					it = s.begin();
				}
				else
				{

					Operate(*(it - 1));
					s = s.substr(2); // assumes operand is a single char
					it = s.begin();

				}
			}
		}
		Operate(*(it - 1));
	}
	catch(const char* e)
	{
		cout << e;
		memory.Clear();
	}

}

void RpnCalculator::Operate(char c)
{
	// checks for too many operators
	if(c != '=' && memory.GetSize() < 2)
		throw "ERROR: Too many operators";
	switch (c)
	{
	case '+':
		Add();
		break;
	case '-':
		Subtract();
		break;
	case '*':
		Multiply();
		break;
	case '/':
		Divide();
		break;
	case '=':
		Output();
		break;
	}


}

void RpnCalculator::Add()
{
	memory.Pop(opR);
	memory.Pop(opL);
	memory.Push(opL + opR);
}

void RpnCalculator::Subtract()
{
	memory.Pop(opR);
	memory.Pop(opL);
	memory.Push(opL - opR);
}

void RpnCalculator::Multiply()
{
	memory.Pop(opR);
	memory.Pop(opL);
	memory.Push(opL * opR);
}

void RpnCalculator::Divide()
{
	memory.Pop(opR);
	memory.Pop(opL);
	// checks for divide by zero
	if (opR == 0)
		throw "ERROR: Divide by zero";
	memory.Push(opL / opR);
}

void RpnCalculator::Output()
{
	// checks for too many operands
	if(memory.GetSize() > 1)
		throw "ERROR: Too many operands";
	cout << memory.Head();
	memory.Clear();
}


>>>>>>> origin/WorkingSets
