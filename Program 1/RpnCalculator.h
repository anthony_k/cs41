<<<<<<< HEAD
/*
 * RpnCalculator
 * 	Reverse Polish Notation calculator that takes a string expression
 * 	terminated by an '='.  Operates with a link list based stack.
 *
 * 	Author: Anthony Koutroulis
 */

#ifndef RPNCALCULATOR_H_
#define RPNCALCULATOR_H_
#include <string>
#include <stdlib.h>
#include "LinkedStack.h"


class RpnCalculator
{
public:
	RpnCalculator();	// Default constructor
	~RpnCalculator();	// Default destructor
	void ClearMemory();		// Returns the stack to an empty state
	void ReadExpression(std::string s);	// Parses an expression and calls
										//	 appropriate operations

private:
	void Operate(char c);	// Calls an operation based on the character
	void Add();		// Pop two, add, and push sum
	void Subtract();	// Pop two, subtract, and push difference
	void Multiply();	// Pop two, multiply, and push product
	void Divide();		// Pop two, divide, and push quotient
	void Output();		// Pop result, outputs, clears the stack

	float opL;	// left operand
	float opR;	// right operand
	LinkedStack<float> memory;	// link based stack to serve as memory
};


#endif /* RPNCALCULATOR_H_ */

#include "LinkedStack.cpp"
=======
/*
 * RpnCalculator
 * 	Reverse Polish Notation calculator that takes a string expression
 * 	terminated by an '='.  Operates with a link list based stack.
 *
 * 	Author: Anthony Koutroulis
 */

#ifndef RPNCALCULATOR_H_
#define RPNCALCULATOR_H_
#include <string>
#include <stdlib.h>
#include "LinkedStack.h"


class RpnCalculator
{
public:
	RpnCalculator();	// Default constructor
	~RpnCalculator();	// Default destructor
	void ClearMemory();		// Returns the stack to an empty state
	void ReadExpression(std::string s);	// Parses an expression and calls
										//	 appropriate operations

private:
	void Operate(char c);	// Calls an operation based on the character
	void Add();		// Pop two, add, and push sum
	void Subtract();	// Pop two, subtract, and push difference
	void Multiply();	// Pop two, multiply, and push product
	void Divide();		// Pop two, divide, and push quotient
	void Output();		// Pop result, outputs, clears the stack

	float opL;	// left operand
	float opR;	// right operand
	LinkedStack<float> memory;	// link based stack to serve as memory
};


#endif /* RPNCALCULATOR_H_ */

#include "LinkedStack.cpp"
>>>>>>> origin/WorkingSets
