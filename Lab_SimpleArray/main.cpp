/*
 * Author:	Anthony Koutroulis
 * Lab:		SimpleArray
 */

#include<iostream>;
using namespace std;

int main()
{
	const int AR_SIZE = 5;
	int numAr[AR_SIZE];

	for(int i = 0; i < AR_SIZE; i++)
	{
		cout << "Enter a number: ";
		cin >> numAr[i];
	}

	cout << "The array contains the numbers: ";
	for(int i = 0; i < AR_SIZE; i++)
	{
		cout << numAr[i] << ", ";
	}

	return 0;
}



