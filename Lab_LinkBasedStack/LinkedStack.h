/*
 * LinkedStack.h
 * 	Contains the template class declarations for a linked list based stack
 *
 * 		Author: Anthony Koutroulis
 *
 */

#ifndef LINKEDSTACK_H_
#define LINKEDSTACK_H_

template <class Type>
struct Node
{
	Type data;			// Data stored in the node
	Node<Type> *next;	// Pointer to the next node
};

template <class Type>
class LinkedStack
{
public:
	// Default Constructor
	LinkedStack(){
		head = NULL;
		size = 0;
	}

	// Default Destructor
	~LinkedStack(){
		this->Clear();
	}

	// Removes all elements from the stack
	void Clear(){
		Node<Type>* ptr;
			this->size = 0;

			while(!this->IsEmpty())
			{
				ptr = head;
				head = head->next;
				delete ptr;
			}
	}

	// Adds a new element to the stack
	void Push(Type newData){
		Node<Type>* ptr;
		ptr = new Node<Type>;
		ptr->data = newData;
		ptr->next = head;
		head = ptr;
		this->size++;
	}

	// Removes the top node from the stack
	void Pop(){
		if(!this->IsEmpty())
		{
			Node<Type>* ptr;
			ptr = head;
			head = head->next;
			delete ptr;
			this->size--;
		}
	}

	// Removes the top node (makes a copy of its data)
	void Pop(Type& copy){
		if(!this->IsEmpty())
		{
			Node<Type>* ptr;
			copy = head->data;
			ptr = head;
			head = head->next;
			delete ptr;
			this->size--;
		}
	}

	// RETURNS if the stack is empty
	bool IsEmpty() const{
		return(size == 0);
	}

	// RETURNS the data at the top of the stack
	Type Head(){
		return head->data;
	}

	// RETURNS the number of nodes in the stack
	int GetSize() const{
		return size;
	}

protected:
	Node<Type>* head;	// The first node in the stack
	int size;			// The number of nodes in the stack
};


#endif /* LINKEDSTACK_H_ */
