
/*
 * LinkedStack.h
 * 	Contains the template class definitions for a linked list based stack
 *
 * 		Author: Anthony Koutroulis
 */

#include <iostream>
#include "LinkedStack.h"
using namespace std;

template <class Type>
LinkedStack<Type>::LinkedStack()
{
	head = NULL;
	size = 0;
}

template <class Type>
LinkedStack<Type>::~LinkedStack()
{
	this->Clear();
}

template <class Type>
void LinkedStack<Type>::Clear()
{
	Node<Type>* ptr;
	this->size = 0;

	while(!this->IsEmpty())
	{
		ptr = head;
		head = head->next;
		delete ptr;
	}
}

template <class Type>
void LinkedStack<Type>::Push(Type newData)
{
	Node<Type>* ptr;
	ptr = new Node<Type>;
	ptr->data = newData;
	ptr->next = head;
	head = ptr;
	this->size++;
}

template <class Type>
void LinkedStack<Type>::Pop()
{
	if(!this->IsEmpty())
	{
		Node<Type>* ptr;
		ptr = head;
		head = head->next;
		delete ptr;
		this->size--;
	}
}

template <class Type>
void LinkedStack<Type>::Pop(Type& data)
{
	if(!this->IsEmpty())
	{
		Node<Type>* ptr;
		data = head->data;
		ptr = head;
		head = head->next;
		delete ptr;
		this->size--;
	}

}

template <class Type>
bool LinkedStack<Type>::IsEmpty() const
{
	return(size == 0);
}

template <class Type>
Type LinkedStack<Type>::Head()
{
	return head->data;
}

template <class Type>
int LinkedStack<Type>::GetSize() const
{
	return(size);
}

template class LinkedStack<float>;
