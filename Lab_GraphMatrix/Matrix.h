/*	Lab_GraphMatrix	************************************************************
 * Create a matrix based graph representation.
 *	It will need to support the following operations.
 *	Ask the user how many points there are.
 *	Ask the user to label those points, ie "A", "B", "C"...
 *	Define the matrix as a square matrix based on the number of points,
 *	 also keep an array of the labels.
 *	Repeatedly ask the user to define edges between two points.
 *	Add these edges to the matrix.
 *	Have a list method that will list out all of the edges in the graph.
 ******************************************************************************/

#ifndef MATRIX_H_
#define MATRIX_H_
#include <iostream>

template<typename VType,typename EType,int SIZE>
class Matrix
{
public:
	Matrix()
	{
		for(int i = 0; i < SIZE; i++)
		{
			for(int j = 0; j < SIZE; j++)
			{
				adjacency_matrix[i][j] = false;
			}
		}
	}

	~Matrix()
	{
		delete [] adjacency_matrix;
	}

	bool IsAdjacent(int i,int j) const
	{
		return adjacency_matrix[i][j];
	}


	void ListEdges()
	{
		for(int i = 0; i < SIZE; i++)
		{
			std::cout << get_vertex_data(i) << '\t';
		}
	}



	void set_vertex_data(int vertex, VType data)
	{
		vertex_data[vertex] = data;
	}

	void set_edge_data(int edge, EType data)
	{
		edge_data[edge] = data;
	}

	VType get_vertex_data(int vertex)
	{
		return vertex_data[vertex];
	}

	EType get_edge_data(int edge)
	{
		return edge_data[edge];
	}

private:

	bool adjacency_matrix[SIZE][SIZE];
	VType vertex_data[SIZE];
	EType edge_data[SIZE];
};




#endif /* MATRIX_H_ */
