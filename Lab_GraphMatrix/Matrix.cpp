#include <iostream>
#include <string>


using namespace std;

int main()
{
	int size = 0;
	int source = 0;
	int destination = 0;
	bool invalid;
	bool defining;
	char* labels;
	bool** matrix;
	char response;

	cout << "Enter number of vertices: ";
	cin >> size;
	cin.ignore(10000,'\n');

	if(size > 0)
	{
		labels = new char[size];
		matrix = new bool*[size];

		for(int i = 0; i < size; i++)
		{
			cout << "Enter label for vertex " << i << ": ";
			cin.get(labels[i]);
			cin.ignore(10000,'\n');

			matrix[i] = new bool[size];
			for(int j = 0; j < size; j++)
			{
				matrix[i][j] = false;
			}
		}

		cout << "\nDefine edges.\n\n";
		defining = true;
		while(defining)
		{
			do
			{
				invalid = false;
				cout << "Enter source index: ";
				cin >> source;

				if(source < 0 || source >= size)
				{
					cout << "Index out of range!\n";
					invalid = true;
				}
			}while(invalid);

			do
			{
				invalid = false;
				cout << "Enter destination index: ";
				cin >> destination;

				if(destination < 0 || destination >= size)
				{
					cout << "Index out of range!\n";
					invalid = true;
				}
			}while(invalid);

			matrix[source][destination] = true;
			if(matrix[source][destination])
			{
				cout << "Linked!\n";
			}
			else
			{
				cout << "Unlinked!\n";
			}

			do
			{
				cout << "Continue(Y/N)? ";
				cin.ignore(1000,'\n');
				cin.get(response);

			}while(toupper(response) != 'Y' && toupper(response) != 'N');

			if(toupper(response) == 'N')
			{
				defining = false;
			}
		}

		for(int i = 0; i < size; i++)
		{
			for(int j = 0; j < size; j++)
			{
				if(matrix[i][j])
				{
					cout << labels[i] << " -> " << labels[j];
					cout << endl;
				}
			}
		}

	}

	return 0;
}
