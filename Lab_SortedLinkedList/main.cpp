/*******************************************************************************
 * AUTHOR: 		Anthony Koutroulis
 *
 * OBJECTIVE: 	Create a sorted singly linked list of numbers based upon
 * 				user input.
 *
 * LOGIC:  		Ask for a number, add that number to the list in sorted position,
 * 				print the list.	Repeat until they enter -1 for the number.
 ******************************************************************************/
#include "SortedLL.h"
#include <iostream>
using namespace std;

int main()
{
	SortedLL list;
	int input;

	cout << "ENTER: ";
	cin >> input;

	while(input != -1)
	{
		list.AddSorted(input);
		cout << endl;
		list.PrintList();

		cout << "\nENTER: ";
		cin >> input;
	}

	return 0;
}
