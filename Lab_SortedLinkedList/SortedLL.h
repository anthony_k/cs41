/*******************************************************************************
 * AUTHOR: 		Anthony Koutroulis
 *
 * OBJECTIVE: 	Create a sorted singly linked list of numbers based upon
 * 				user input.
 *
 * LOGIC:  		Ask for a number, add that number to the list in sorted position,
 * 				print the list.	Repeat until they enter -1 for the number.
 ******************************************************************************/

#ifndef SORTEDLL_H_
#define SORTEDLL_H_

// Integer implementation
struct Node
{
	int data;
	Node* next;

	Node(int i){
		data = i;
		next = 0;
	}
	Node(int i, Node* n){
		data = i;
		next = n;
	}

};

class SortedLL
{
public:
	SortedLL();
	~SortedLL();

	void AddSorted(int);
	void PrintList();

private:
	unsigned int size;
	Node* head;
	Node* ptr;
};





#endif /* SORTEDLL_H_ */
