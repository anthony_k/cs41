/*******************************************************************************
 * AUTHOR: 		Anthony Koutroulis
 *
 * OBJECTIVE: 	Create a sorted singly linked list of numbers based upon
 * 				user input.
 *
 * LOGIC:  		Ask for a number, add that number to the list in sorted position,
 * 				print the list.	Repeat until they enter -1 for the number.
 ******************************************************************************/
#include "SortedLL.h"
#include<iostream>

SortedLL::SortedLL()
{
	size = 0;
	head = NULL;
	ptr = NULL;
}

SortedLL::~SortedLL()
{
	while(head != NULL)
	{
		ptr = head;
		head = head->next;
		delete ptr;
	}
	ptr = NULL;
}

void SortedLL::AddSorted(int i)
{
	if(head == NULL)
	{
		head = new Node(i);
	}
	else if(i < head->data)
	{
		head = new Node(i,head);
	}
	else
	{
		bool found = false;
		ptr = head;

		while(ptr->next != NULL && !found)
		{
			if(i < ptr->next->data)
			{
				found = true;
			}
			else
			{
				ptr = ptr->next;
			}
		}
		ptr->next = new Node(i,ptr->next);
	}
	ptr = NULL;
}

void SortedLL::PrintList()
{
	ptr = head;
	while(ptr != NULL)
	{
		std::cout << ptr->data;
		std::cout << '\t';
		ptr = ptr->next;
	}
}
