<<<<<<< HEAD
/*
 * AUTHOR:	Anthony Koutroulis
 * CLASS:	CS41 Data Structures
 * LAB:		Dynamic Arrays
 * DUE: 	2/08/15
 */
#include <iostream>
using namespace std;

// PopulateIntArray populates an integer array with integers entered by the
//	user from the console, exiting if the user enters an integer that
//	matches TERM_VAL. Calls ResizeIntArray to double the array size if the
//	user has exceeded the previously set array size.
//	RETURNS the populated array
int* PopulateIntArray(int array[], int& size, int index, const int TERM_VAL);

// ResizeIntArray resizes the integer array passed by reference, to the new
// size passed in as a parameter
void ResizeIntArray(int* &array, const int size, const int index);

int* PopulateIntArray(int array[], int& size, int index, const int TERM_VAL)
{
	int input = 0;

	cout << "Enter element for index "	<< index
			<< "\n(Enter " << TERM_VAL << " to quit)\n\n";
	cin >> input;

	// Populates until array size is exceeded or user enters the TERM_VAL
	while (input != TERM_VAL && index < size)
	{
		array[index] = input;
		index++;

		cout << "Enter element for index "	<< index
				<< "\n(Enter " << TERM_VAL << " to quit)\n\n";
		cin >> input;
	}

	// If the size has been exceed, ResizeIntArray is called and the array
	//	is updated with the previously entered values.
	if (input != TERM_VAL && index >= size)
	{
		cout << "ARRAY FULL. Resizing...\n\n";
		size = size * 2;
		ResizeIntArray(array, size, index);
		array[index] = input;
		index++;

		// Recursive call to PopulateIntArray to continue populating array
		PopulateIntArray(array, size, index, TERM_VAL);
	}
	// If the user has quit and the array is over-sized, ResizeIntArray is
	//	called to shrink the array
	else if (input == TERM_VAL && index < size)
	{
		cout << "ARRAY NOT FULL. Resizing...\n\n";
		size = index;
		ResizeIntArray(array, size, index);
	}

	return array;
}

void ResizeIntArray(int* &array, int size, int index)
{
	int* tempAr = NULL;
	tempAr = new int[size];
	for(int i = 0; i < size; i++)
		tempAr[i] = 0;
	for(int i = 0; i < index; i++)
	{
		tempAr[i] = array[i];
	}
	delete [] array;
	array = tempAr;
	tempAr = NULL;
}

int main()
{
	const int TERM_VAL = -1;
	int* intAr;
	int arSize = 0;
	int index = 0;

	do	// make sure the user enters a positive nonzero array size
	{
		cout << "Enter size for integer array: ";
		cin >> arSize;

		if(arSize > 0)
			intAr = new int[arSize];

	}while(arSize <= 0);

	// initialize array members to 0
	for(int i = 0; i < arSize; i++)
		intAr[i] = 0;

	// populates the array
	intAr = PopulateIntArray(intAr, arSize, index, TERM_VAL);

	// OUTPUTs the final array.
	for(int i = 0; i < arSize; i++)
		cout << '[' << intAr[i] << ']';

}



=======
/*
 * main.cpp
 *
 *  Created on: Jan 2, 2015
 *      Author: Anthony
 */
#include <iostream>
using namespace std;

template <class Type>
void PopulateArray(Type* array, int size, const Type TERM_VAL)
{
	int index;
	index = array->size - size;
	int input = 0;

	cout << "Enter element for index "	<< index
			<< "\n(Enter " << TERM_VAL << " to quit)\n\n";
	cin >> input;

	while (input != TERM_VAL && index < size)
	{
		array[index] = input;
		index++;

		cout << "Enter element for index "	<< index
				<< "\n(Enter " << TERM_VAL << " to quit)\n\n";
		cin >> input;
	}

	if (input != TERM_VAL)
	{
		ResizeIntArray(array, size + 1);

	}
}

void ResizeIntArray(int* array, int size)
{
	int* newArray;


}

int main()
{
	int* intArA;
	int* intArB;
	int arSize = 0;
	int index = 0;
	int input = 0;

	cout << "Enter array size: ";
	cin >> arSize;
	intArA = new int[arSize];

	cout << "Enter element for index "	<< index
			<< "\n(Enter -1 to quit)\n\n";
			cin >> input;

	while (input != -1 && index < arSize)
	{
		intArA[index] = input;
		index++;

		cout << "Enter element for index "	<< index
						<< "\n(Enter -1 to quit)\n\n";
		cin >> input;
	}

	if (index == arSize)
	{

	}


}



>>>>>>> origin/WorkingSets
