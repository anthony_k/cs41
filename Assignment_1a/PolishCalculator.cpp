/*
 * PolishCalculator.cpp
 *
 *  Created on: Sep 22, 2014
 *      Author: Anthony
 */

#include "PolishCalculator.h"
using namespace std;

PolishCalculator::PolishCalculator()
{
	op1 = 0;
	op2 = 0;
	result = 0;
	token = ' ';
	invalid = false;
}

PolishCalculator::~PolishCalculator()
{
	stack.Clear();
}

void PolishCalculator::Read(istream& in)
{
	string entry;
	int it;

	cerr << 1;
	getline(in, buffer);
	cerr << buffer << endl;

	while(!invalid)
	{
		while(buffer[it] != ' ')
		{
			entry += buffer[it];
			it++;
		}
		cerr << entry;

		if(isdigit(entry[0]) || isdigit(entry[1]))
		{
			stack.Push(atof(entry.c_str()));
			cerr << "\nPushing: " << stack.Head() << endl;
		}
		else
		{
			token = entry[0];
			cerr << "\nOperating with: " << token;
			Operate();
		}
	//	i++;
	//	entry.clear();

	}
}

void PolishCalculator::Output(ostream& out)
{
	if(invalid)
	{
		if(op2 == 0)
			out << "ERROR: Divide by zero";
		else if(stack.GetSize() > 1)
			out << "ERROR: Too many operands";
		else
			cout << "ERROR: Too many operators";
	}
	else
	{
		out << result;
	}
}

void PolishCalculator::Operate()
{
	if(stack.GetSize() >= 2)
	{
		switch(token)
		{
		case '+':
			{
				stack.Pop(op2);
				stack.Pop(op1);
				stack.Push(op1 + op2);
				break;
			}
		case '-':
			{
				stack.Pop(op2);
				stack.Pop(op1);
				stack.Push(op1 - op2);
				break;
			}
		case '*':
			{
				stack.Pop(op2);
				stack.Pop(op1);
				stack.Push(op1 * op2);
				break;
			}
		case '/':
			{
				stack.Pop(op2);
				if(op2 != 0)
				{
					stack.Pop(op1);
					stack.Push(op1 / op2);
				}
				else
					invalid = true;
				break;
			}
		case '=':
			{
				stack.Pop(result);
				break;
			}
		}
	}
	else
	{
		invalid = true;
	}

}


