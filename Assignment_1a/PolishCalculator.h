/*
 * PolishCalculator.h
 *
 *  Created on: Sep 22, 2014
 *      Author: Anthony
 */

#ifndef ASSIGNMENT_1_POLISHCALCULATOR_H_
#define ASSIGNMENT_1_POLISHCALCULATOR_H_

#include "LinkedStack.h"
#include "LinkedStack.cpp"
#include <string>
#include <stdlib.h>
using namespace std;

class PolishCalculator
{
public:
	PolishCalculator();
	~PolishCalculator();
	void Read(istream& in);
	void Output(ostream& out);


protected:
	LinkedStack<double> stack;

private:
	void Operate();
	bool invalid;
	string buffer;
	double op1, op2;
	double result;
	char token;
};



#endif /* ASSIGNMENT_1_POLISHCALCULATOR_H_ */
