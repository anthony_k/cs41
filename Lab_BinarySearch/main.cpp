#include <iostream>
using namespace std;

template<class Type, unsigned int Size>
int BinarySearch(const Type, const Type);

template<class Type, unsigned int Size>
int BinarySearch(const Type array[], const Type key)
{
	int left = 0;
	int right = Size -1;
	int middle;

	while(left <= right)
	{
		middle = (left + right)/2;
		if(key > array[middle])
		{
			left = middle + 1;
		}
		else if(key < array[middle])
		{
			right = middle - 1;
		}
		else
		{
			return middle;
		}
	}

	return -1;
}



int main()
{
	const int AR_SIZE = 10;
	int ar[AR_SIZE] = {-1,0,2,4,5,6,8,9,10,12};

	cout << "2 is at index: " << BinarySearch<int, AR_SIZE>(ar, 2) << endl;
	cout << "-1 is at index: " << BinarySearch<int, AR_SIZE>(ar, -1) << endl;
	cout << "6 is at index: " << BinarySearch<int, AR_SIZE>(ar, 6) << endl;
	cout << "12 is at index: " << BinarySearch<int, AR_SIZE>(ar, 12) << endl;
	cout << "7 is at index: " << BinarySearch<int, AR_SIZE>(ar, 7) << endl;


	return 0;
}
