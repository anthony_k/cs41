/**************************************************************************
 * NAME: Anthony Koutroulis
 * LAB:	 Loops - IO
 * 	Reads a sentence from the console, breaking the sentence into words
 * using the space character as a delimiter. Iterates over each word,
 * printing the it on a newline, and printing double the value of numeric
 * characters
 *************************************************************************/
#include<string>
#include<iostream>
#include<stdlib.h>
using namespace std;


int main()
{
	// VARIABLE LIST
	string line;	// IN - Line to be parsed
	string word;	// OUT - Holds a word to output
	double num;		// OUT - Holds a number to output

	// INITIALIZATION
	if(!word.empty())
		word.clear();
	if(!line.empty())
		line.clear();
	num = 0;

	// INPUT - Prompts the user for a line to parse
	cout << "Enter a sample line: ";
	getline(cin, line);

	// PROCESSING - Iterates over every character in the string adding each
	//				a word until a space or null terminator is reached
	for(unsigned int i = 0; i <= line.size(); i++)
	{
		if(line[i] == ' ' || line[i] == '\0')
		{
			// Checks for a digit or a negative sign followed by a digit
			if(isdigit(word[0]) ||
			(word[0] == '-' && isdigit(word[1])))
			{
				num = atof(word.c_str());
				num *= 2;

				// OUTPUT - outputs the number (times two)
				cout << num << endl;
			}
			else
			{
				// OUTPUT - outputs the word
				cout << word << endl;
			}
			word.clear();
			num = 0;
		}
		else
		{
			word += line[i];
		}
	}

	return 0;
}
