template<class Type>void ArraySort(Type,const int);
template<class Type>void ArraySort(Type array, const int AR_SIZE)
{
	int i = 0;
	int j = 0;
	int temp;

	for(int i = 0; i < AR_SIZE; i++)
	{
		for(int j = 0; j < AR_SIZE-1; j++)
		{
			if(array[j] > array[j+1])
			{
				temp = array[j];
				array[j] = array[j+1];
				array[j+1] = temp;
			}
		}
	}
}


#include <iostream>
int main()
{
	const int AR_SIZE = 10;
	int ar[AR_SIZE] = {2,4,1,6,8,0,3,2,-1,9};

	ArraySort<int*>(ar, AR_SIZE);

	for(int i = 0; i < AR_SIZE; i++)
	{
		std::cout << ar[i] << ' ';
	}

	return 0;
}
