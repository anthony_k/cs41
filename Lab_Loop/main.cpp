<<<<<<< HEAD
/*
 *	Author: Anthony
 *	Lab: Loops
 */


//Loops: Create a method that uses a loop to calculate the summation.
int summation(int n); //NOTE.. slooow
int summation(int n)
{
	int sum;
	sum = 0;

	while(n > 0)
	{
		sum += n;
		n--;
	}

	return sum;
}

int sum(int n); //NOTE.. faster
int sum(int n)
{
	if (n < 1)
		return 0;

	return sum(n-1) + n;
}

int sumFormula(int n); //NOTE.. math
int sumFormula(int n)
{
	return n*(n+1)/2;
}

#include<iostream>
using namespace std;

int main()
{
	int test;
	int number;

	cout << "Enter a number: ";
	cin >> number;
	test = sumFormula(number);
	cout << "The sum is: " << test;
	test = summation(number);
	cout << "\nThe sum is: " << test;
	cin.get();

	return 0;
}
=======
/*
 *	Author: Anthony
 *	Lab: Loops
 */


//Loops: Create a method that uses a loop to calculate the summation.
int summation(int n); //NOTE.. slooow
int summation(int n)
{
	int sum;
	sum = 0;

	while(n > 0)
	{
		sum += n;
		n--;
	}

	return sum;
}

int sum(int n); //NOTE.. faster
int sum(int n)
{
	if (n < 1)
		return 0;

	return sum(n-1) + n;
}

int sumFormula(int n); //NOTE.. math
int sumFormula(int n)
{
	return n*(n+1)/2;
}

#include<iostream>
using namespace std;

int main()
{
	int test;
	int number;

	cout << "Enter a number: ";
	cin >> number;
	test = sumFormula(number);
	cout << "The sum is: " << test;
	test = summation(number);
	cout << "\nThe sum is: " << test;
	cin.get();

	return 0;
}
>>>>>>> origin/WorkingSets
