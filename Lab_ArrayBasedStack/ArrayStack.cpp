/*
 * AUTHOR:	Anthony Koutroulis
 * CLASS:	CS41 Data Structures
 * LAB:		Array Based Stack
 * DUE: 	2/08/15
 */
#include "ArrayStack.h"
#include <iostream>


template <class Type>
ArrayStack<Type>::ArrayStack()
{
	Stack = new Type[MAX_SIZE];
	Count = 0;
	for(int i = 0; i < MAX_SIZE; i++)
		Stack[i] = 0;
}

template <class Type>
ArrayStack<Type>::~ArrayStack()
{
	delete [] Stack;
}

template <class Type>
void ArrayStack<Type>::Push(Type data)
{
	if (Count == MAX_SIZE)
	{
		std::cerr << "ERROR: Stack is full.\n"
				<< data << " was not added!\n";
	}
	else
	{
		Stack[Count] = data;
		Count++;
	}
}

template <class Type>
void ArrayStack<Type>::Pop(Type& copy)
{
	if (IsEmpty())
	{
		std::cerr << "ERROR: Stack is empty.\n"
				<< "Nothing to Pop!\n";
	}
	else
	{
		copy = Stack[Count];
		Stack[Count] = 0;
		Count--;
	}
}

template <class Type>
Type ArrayStack<Type>::Pop()
{
	if (IsEmpty())
	{
		std::cerr << "ERROR: Stack is empty.\n"
				<< "RETURNING -1!\n";
		return -1;
	}
	else
	{
		return Stack[--Count];
	}
}

template <class Type>
bool ArrayStack<Type>::IsEmpty() const
{
	return Count == 0;
}

template <class Type>
int ArrayStack<Type>::MaxSize() const
{
	return MAX_SIZE;
}

