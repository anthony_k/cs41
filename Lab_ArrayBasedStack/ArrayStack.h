/*
 * AUTHOR:	Anthony Koutroulis
 * CLASS:	CS41 Data Structures
 * LAB:		Array Based Stack
 * DUE: 	2/08/15
 */

#ifndef ARRAYSTACK_H_
#define ARRAYSTACK_H_


template <class Type>
class ArrayStack
{
public:
	ArrayStack();
	~ArrayStack();
	void Push(Type);
	void Pop(Type&);
	Type Pop();
	bool IsEmpty() const;
	int MaxSize() const;

private:
	const int MAX_SIZE = 10;
	Type* Stack;
	int Count;
};


#endif /* ARRAYSTACK_H_ */
