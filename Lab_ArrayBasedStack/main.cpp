/*
 * main.cpp
 *
 *  Created on: Feb 7, 2015
 *      Author: Anthony
 */
#include "ArrayStack.h"
#include "ArrayStack.cpp"
#include <iostream>
using namespace std;

int main()
{
	ArrayStack<int> arStack;
	int n;


	while(true)
	{
		cout << "1. Push\n"
				"2. Pop\n"
				"3. IsEmpty\n";
		cin >> n;

		switch(n)
		{
		case 1 :
			cout << "Enter: ";
			cin >> n;
			arStack.Push(n);
			break;
		case 2:
			cout << arStack.Pop() << " was popped\n";
			break;
		case 3:
			arStack.IsEmpty() == 1 ?
					cout << "EMPTY\n"
					: cout << "NOT EMPTY\n";
			break;

		}
	}
}



