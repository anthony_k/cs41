// Anthony Koutroulis 3/15/15

#include "BinarySearchTree.h"
#include "BinarySearchTree.cpp"
#include<iostream>

using namespace std;

int main()
{
	BinarySearchTree<int> bst;;
	bst.Add(3);
	bst.Add(1);
	bst.Add(6);
	bst.Add(4);
	bst.Add(7);
	bst.Add(10);
	bst.Add(14);
	bst.Add(13);
	bst.Print();

	return 0;
}



