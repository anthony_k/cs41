// Anthony Koutroulis 3/15/15

#include "BinarySearchTree.h"

template<class Type>
BinarySearchTree<Type>::BinarySearchTree()
{
	root = NULL;
	node = NULL;
}


template<class Type>
BinarySearchTree<Type>::~BinarySearchTree()
{
	// Calls recursive destructor: ~BST_Node()
	delete root;
}


template<class Type>
void BinarySearchTree<Type>::Add(Type data)
{
	node = new BST_Node<Type>(data);

	if(node == NULL)
	{
		std::cout << "INSUFFICIENT MEMORY\n";
	}
	else
	{
		// Determine where the node may go
		if(root == NULL)
		{
			// First case, set to root
			root = node;
			node = NULL;
		}
		else if(node->data < root->data)
		{
			// Start down left subtree
			node->parent = root;
			Add_Sorted(root->left);
		}
		else if(node->data > root->data)
		{
			// Start down right subtree
			node->parent = root;
			Add_Sorted(root->right);
		}
		else // node->data == root->data
		{
			// Already in tree, discard
			delete node;
		}

	}
}

template<class Type>
void BinarySearchTree<Type>::Add_Sorted(BST_Node<Type>*& child)
{
	if(child == NULL)
	{
		// Place at this leaf
		child = node;
	}
	else if(node->data < child->data)
	{
		// Traverse left
		node->parent = child;
		Add_Sorted(child->left);
	}
	else if(node->data > child->data)
	{
		// Traverse right
		node->parent = child;
		Add_Sorted(child->right);
	}
	else // node->data == root->data
	{
		// Free memory
		delete node;
	}
}

template<class Type>
void BinarySearchTree<Type>::Print()
{
	if(root == NULL)
	{
		std::cout << "NOTHING TO PRINT\n";
	}
	else
	{
		// Recursively prints in-order
		node = root;
		Print_InOrder();
	}

}

template<class Type>
void BinarySearchTree<Type>::Print_InOrder()
{
	if(node->left != NULL)
	{
		node = node->left;
		Print_InOrder();
	}

	std::cout << node->data << '\t';

	if(node->right != NULL)
	{

		node = node->right;
		Print_InOrder();
	}

	node = node->parent;

	return;
}
