/*
 * Binary Search Tree
 * 	Does not accept duplicates
 *
 * 	Anthony Koutroulis 3/15/15
 */

#ifndef BINARYSEARCHTREE_H_
#define BINARYSEARCHTREE_H_
#include<iostream>

template<class Type>
struct BST_Node
{
	Type data;			// data field
	BST_Node* parent;	// pointer to parent
	BST_Node* left;		// pointer to left child
	BST_Node* right;	// pointer to right child

	BST_Node(Type key)
	{
		left = right = parent = NULL;
		data = key;
	}
	~BST_Node()
	{
		delete left;	// recursively delete left subtree
		delete right;	// recursively delete right subtree
	}
};

template<class Type>
class BinarySearchTree
{
public:
	BinarySearchTree(){
		root = NULL;
		node = NULL;
	}
	~BinarySearchTree();

	void Add(Type);	// Adds the data to the tree in sorted order
	void Print();	// Prints all the nodes using in-order traversal


private:
	void Print_InOrder();				// recursive inorder print
	void Add_Sorted(BST_Node<Type>*&);	// recursively find spot for new node

	BST_Node<Type>* root;		// Node field for the topmost member
	BST_Node<Type>* node;		// Node field for new members
};



#endif /* BINARYSEARCHTREE_H_ */
