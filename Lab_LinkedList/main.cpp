/**************************************************************************
 * Author:	Anthony Koutroulis
 * Class:	CS41
 * Lab:		Linked List
 * Due:		8/31/14
 *************************************************************************/


/**************************************************************************
Objective:  Create a singly linked list of numbers based upon user input.
Program logic:
Ask for a number, add that number to the front of the list, print the list.
Repeat until they enter -1 for the number.
---------------------------------------------------------------------------
Sample Input: 10, 15, 5, 2, 4, -1
Output: 4, 2, 5, 15, 10
**************************************************************************/
#include<iostream>
using namespace std;

/**************************************************************************
 * NumberNode:
 * 	Structure representing a node to be used in a linked list
 *************************************************************************/
struct NumberNode
{
// Attributes:
	int data;			// IN,OUT,CAL - holds the integer value of the node
	NumberNode* prev;	// CAL - pointer to the previous node in the list
};


int main()
{
	NumberNode* head;	// CAL - pointer to the first node in the list
	NumberNode* ptr;	// CAL - temporary pointer for operating on the list
	int number;			// IN - number to be stored in a node

	head = NULL;
	ptr = NULL;
	number = 0;

	// INPUT - prompt the user for an integer and store the integer
	cout << "Enter a number: ";
	cin >> number;

	// CALC - builds the list of integers until -1 is inputted
	while(number != -1)
	{
		ptr = head;
		head = new NumberNode;
		head->prev = ptr;
		head->data = number;
		ptr = head;

		// OUTPUT - traverses the list, outputting the value of each node
		while(ptr != NULL)
		{
			cout << ptr->data << " --> ";
			ptr = ptr->prev;
		}
		cout << "NULL" << endl;

		// INPUT - prompts the user for a new integer
		cout << "Enter a number: ";
		cin >> number;
	}

	ptr = NULL;
	delete ptr;

	return 0;
}
